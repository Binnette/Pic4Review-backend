/*
 * Test script for model/dataset/Osmose.js
 */

global.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
global.XMLHttpRequest.DONE = 4;

const assert = require('assert');
const Osmose = require('../../../src/model/dataset/Osmose');
const P4C = require('pic4carto');
const LatLng = P4C.LatLng;
const LatLngBounds = P4C.LatLngBounds;

const TIMEOUT = 10000;

describe("Model > Dataset > Osmose", () => {
	describe("Constructor", () => {
		it("works with valid parameters", () => {
			const d1 = new Osmose(1070, 10);
			assert.equal(d1.searchOptions.item, 1070);
			assert.equal(d1.searchOptions.limit, 10);
		});
		
		it("fails if no item ID is given", () => {
			assert.throws(() => {
				new Osmose();
			}, TypeError);
		});
		
		it("fails if invalid amount is given", () => {
			assert.throws(() => {
				new Osmose(1070, -1);
			}, TypeError);
		});
		
		it("reads correctly bbox option", () => {
			const d1 = new Osmose(1070, 10, { bbox: new LatLngBounds(new LatLng(1.1,2.2), new LatLng(3.3,4.4)) });
			assert.equal(d1.searchOptions.bbox, "2.2,1.1,4.4,3.3");
		});
	});
	
	describe("getAllFeatures", () => {
		it("returns retrieved features", done => {
			const d1 = new Osmose(3230, 2, { bbox: new LatLngBounds(new LatLng(48.8141, 2.2556), new LatLng(48.9022, 2.4184)) });
			d1.getAllFeatures()
			.then(fts => {
				assert.equal(fts.length, 2);
				assert.ok(fts[0] !== null);
				done();
			})
			.catch(e => {
				assert.fail(e);
				done();
			});
		}).timeout(TIMEOUT);
	});
});

/*
 * Test script for model/dataset/Overpass.js
 */

global.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
global.XMLHttpRequest.DONE = 4;

const assert = require('assert');
const Overpass = require('../../../src/model/dataset/Overpass');
const P4C = require('pic4carto');
const LatLng = P4C.LatLng;
const LatLngBounds = P4C.LatLngBounds;

const REQUEST = '[out:json][timeout:25];node["highway"="traffic_signals"]({{bbox}});out body;';
const TIMEOUT = 10000;

describe("Model > Dataset > Overpass", () => {
	describe("Constructor", () => {
		it("works with valid parameters", () => {
			const d1 = new Overpass(REQUEST);
			assert.equal(d1.query, REQUEST);
		});
		
		it("fails if no query is given", () => {
			assert.throws(() => {
				new Overpass();
			}, TypeError);
		});
		
		it("reads correctly bbox option", () => {
			const d1 = new Overpass('{{bbox}} {{bbox}}', { bbox: new LatLngBounds(new LatLng(48.1263031510851,-1.6871309280395508), new LatLng(48.12790369887754,-1.6858139634132385)) });
			assert.equal(d1.bbox, "48.1263031510851,-1.6871309280395508,48.12790369887754,-1.6858139634132385");
			assert.equal(d1.query, '48.1263031510851,-1.6871309280395508,48.12790369887754,-1.6858139634132385 48.1263031510851,-1.6871309280395508,48.12790369887754,-1.6858139634132385');
		});
	});
	
	describe("getAllFeatures", () => {
		it("returns retrieved features", done => {
			const d1 = new Overpass(REQUEST, { bbox: new LatLngBounds(new LatLng(48.1263031510851,-1.6871309280395508), new LatLng(48.12790369887754,-1.6858139634132385)) });
			d1.getAllFeatures()
			.then(fts => {
				assert.equal(fts.length, 7);
				assert.ok(fts[0] !== null);
				done();
			})
			.catch(e => {
				assert.fail(e);
				done();
			});
		}).timeout(TIMEOUT);
	});
});

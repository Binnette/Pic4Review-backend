const CONFIG = require('../../../config.json');
const Dataset = require('../Dataset');
const Feature = require('../Feature');
const GeoJSON = require('./GeoJSON');
const Hash = require('object-hash');
const queryOverpass = require('query-overpass');

/**
 * An Overpass {@link Dataset} is a set of features retrieved from Overpass API.
 * Overpass is a query system to extract thematic data from OSM.
 * This kind of dataset is dynamic.
 * 
 * @name OverpassDataset
 * @param {string} query The Overpass API query
 * @param {Object} [options] Options for data retrieval
 */
class Overpass extends Dataset {
	constructor(query, options) {
		super();
		
		if(!query || query.trim().length < 5) {
			throw new TypeError("Invalid query");
		}
		
		this.query = query;
		this.options = options || {};
		
		//Convert bbox
		if(this.options.bbox) {
			this.bbox = this.options.bbox.getSouth()+","+this.options.bbox.getWest()+","+this.options.bbox.getNorth()+","+this.options.bbox.getEast();
			this.query = this.query.replace(/{{bbox}}/g, this.bbox);
		}
		
		this.id = Hash(query);
		this.geojson = null;
		
		this._loadData();
	}
	
	/**
	 * Download data from Overpass API
	 * @memberof OverpassDataset
	 * @instance
	 * @private
	 */
	_loadData() {
		this.isDownloading = true;
		
		queryOverpass(this.query, (err, data) => {
			if(err) {
				console.error("[Overpass] API error "+JSON.stringify(err));
			}
			else {
				try {
					this.geojson = new GeoJSON(data);
				}
				catch(e) {
					console.error("[Overpass] GeoJSON error "+JSON.stringify(e));
				}
			}
			
			this.isDownloading = false;
		}, { overpassUrl: CONFIG.overpass, flatProperties: true });
	}
	
	/**
	 * Override of {@link Dataset#getAllFeatures}
	 * @memberof OverpassDataset
	 * @instance
	 */
	getAllFeatures() {
		return new Promise((resolve, reject) => {
			if(this.isDownloading) {
				setTimeout(() => {
					resolve(this.getAllFeatures());
				}, 100);
			}
			else {
				if(this.geojson) {
					resolve(this.geojson.getAllFeatures());
				}
				else {
					reject(new Error("Features not available"));
				}
			}
		});
	}
}

module.exports = Overpass;

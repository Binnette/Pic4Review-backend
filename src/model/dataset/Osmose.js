const Dataset = require('../Dataset');
const Feature = require('../Feature');
const Hash = require('object-hash');
const request = require('request-promise-native');
const API_URL = "https://osmose.openstreetmap.fr/fr/api/0.3beta";

/**
 * An Osmose {@link Dataset} is a set of features retrieved from {@link http://osmose.openstreetmap.fr|Osmose API}.
 * Osmose is a system returning anomalies from OpenStreetMap database. It also supports open data merging with OSM.
 * This kind of dataset is dynamic.
 *
 * @param {int} itemId The Osmose item ID
 * @param {int} amount The amount of errors to review
 * @param {Object} [options] Options for data retrieval
 * @param {int} [options.class] Osmose class for a given item
 * @param {LatLngBounds} [options.bbox] Bounding box for limiting search area
 */
class OsmoseDataset extends Dataset {
	constructor(itemId, amount, options) {
		super();

		this.options = options || {};
		amount = amount || 10000;

		if(isNaN(parseInt(itemId))) {
			throw new TypeError("You should provide a valid item ID");
		}
		else if(isNaN(parseInt(amount)) || amount < 1) {
			throw new TypeError("You should provide a valid amount (integer > 0)");
		}

		this.id = Hash(itemId);
		this.searchOptions = Object.assign({ item: itemId, limit: amount, status: "open", full: true }, this.options);
		delete this.searchOptions.allowedEditors;
		delete this.searchOptions.editors;
		this.features = null;

		//Convert bbox
		if(this.options.bbox) {
			this.searchOptions.bbox = this.options.bbox.getWest()+","+this.options.bbox.getSouth()+","+this.options.bbox.getEast()+","+this.options.bbox.getNorth();
		}

		this._loadData();
	}

	/**
	 * Download data from Osmose API
	 * @private
	 */
	_loadData() {
		this.isDownloading = true;

		// Using beta endpoint
		const url = API_URL+"/issues?" + Object.entries(this.searchOptions).map(e => e[0]+"="+e[1]).join("&");

		request(url)
		.then(res => JSON.parse(res))
		.then(res => res.issues)
		.then(result => {
			this.features = [];
			const validProps = ["title"];

			for(const f of result) {
				//Filter properties to only display what's useful
				const props = {};
				for(const k in f) {
					if(validProps.includes(k)) {
						props[k] = f[k];
					}
					else if(k === "elems" && f[k].trim().length > 0) {
						const osmid = f[k].split("_")[0];
						props["id"] = osmid.replace(/^([a-z]+)(\d+)$/, "$1/$2");
					}
					else if(k === "subtitle" && f[k].trim().length > 0) {
						props["details"] = f[k];
					}
				}

				this.features.push(new Feature(f.id, [ parseFloat(f.lat), parseFloat(f.lon) ], props));
			}

			this.isDownloading = false;
		})
		.catch(e => {
			console.error(e);
			this.isDownloading = false;
		});
	}

	/**
	 * Override of {@link Dataset#getAllFeatures}
	 */
	getAllFeatures() {
		return new Promise((resolve, reject) => {
			if(this.isDownloading) {
				setTimeout(() => {
					resolve(this.getAllFeatures());
				}, 100);
			}
			else {
				if(this.features) {
					resolve(this.features);
				}
				else {
					reject(new Error("Features not available"));
				}
			}
		});
	}

	/**
	 * Retrieve tags to apply to solve an Osmose error
	 * @param {string} id The Osmose error ID
	 * @return {Promise} A promise resolving on tags to apply
	 */
	static getErrorTags(id) {
		const url = API_URL + "/issue/"+id;
		return request(url)
			.then(res => JSON.parse(res))
			.then(result => {
				const tags = {};

				if(result.new_elems && result.new_elems.length > 0 && result.new_elems[0].add) {
					result.new_elems[0].add.forEach(e => {
						tags[e.k] = e.v;
					});
				}

				return tags;
			});
	}

	/**
	 * Mark an error as fixed on Osmose
	 * @param {string} id The Osmose error ID
	 * @return {Promise} A promise resolving when done
	 */
	static setErrorFixed(id) {
		const url = API_URL + "/issue/"+id+"/done";
		return request(url);
	}
}

// Pictures for illustrating missions
OsmoseDataset.ILLUSTRATIONS = {
	"1110": "https://wiki.openstreetmap.org/w/images/b/b5/Osmose-eg-error-1110.png",
	"2010": "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/Greenpoipt_traffic_circle_-_panoramio.jpg/320px-Greenpoipt_traffic_circle_-_panoramio.jpg",
	"2030": "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Lyon_9e_-_Place_Maurice_Bariod_-_Plaque_et_sens_unique_%28f%C3%A9v_2019%29.jpg/320px-Lyon_9e_-_Place_Maurice_Bariod_-_Plaque_et_sens_unique_%28f%C3%A9v_2019%29.jpg",
	"2060": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Num%C3%A9ro_121%2C_Rue_de_Vaugirard_%28Paris%29.jpg/479px-Num%C3%A9ro_121%2C_Rue_de_Vaugirard_%28Paris%29.jpg",
	"2090": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/LED_traffic_light.jpg/304px-LED_traffic_light.jpg",
	"2100": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Znak_%C4%8Cesk%C3%A9_l%C3%A9k%C3%A1rensk%C3%A9_komory.JPG/320px-Znak_%C4%8Cesk%C3%A9_l%C3%A9k%C3%A1rensk%C3%A9_komory.JPG",
	"2120": "https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/It%C3%A4keskus_shopping_centre.JPG/320px-It%C3%A4keskus_shopping_centre.JPG",
	"2140": "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/BYD_K-9_electric_test_bus_0060_in_NYC.jpg/320px-BYD_K-9_electric_test_bus_0060_in_NYC.jpg",
	"3080": "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Building_5885.jpg/318px-Building_5885.jpg",
	"3160": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Gda%C5%84sk_Zaspa_-_trzy_pasy_ruchu_%28ubt%29.jpg/458px-Gda%C5%84sk_Zaspa_-_trzy_pasy_ruchu_%28ubt%29.jpg",
	"3210": "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Sans_issue.svg/238px-Sans_issue.svg.png",
	"3230": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/N%C3%A1b%C5%99e%C5%BE%C3%AD_J._Palacha_-_Foersterova%2C_kontejnery.jpg/320px-N%C3%A1b%C5%99e%C5%BE%C3%AD_J._Palacha_-_Foersterova%2C_kontejnery.jpg",
	"4070": "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/120_inch_HDPE_pipe_installation_in_Mexico_City.jpg/558px-120_inch_HDPE_pipe_installation_in_Mexico_City.jpg",
	"7012": "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/Santa_fe.jpg/320px-Santa_fe.jpg",
	"7040": "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e2/Top_of_power_line_pole_-_west_side.jpg/315px-Top_of_power_line_pole_-_west_side.jpg",
	"7140": "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/%2BDresdner_Verkehrsbetriebe_Service_Point_-_Pirnaischer_Platz_-_MOBI_-_eCAR_Sharing_-_Bild_001.jpg/320px-%2BDresdner_Verkehrsbetriebe_Service_Point_-_Pirnaischer_Platz_-_MOBI_-_eCAR_Sharing_-_Bild_001.jpg",
	"7150": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Znak_%C4%8Cesk%C3%A9_l%C3%A9k%C3%A1rensk%C3%A9_komory.JPG/320px-Znak_%C4%8Cesk%C3%A9_l%C3%A9k%C3%A1rensk%C3%A9_komory.JPG",
	"7190": "https://wiki.openstreetmap.org/w/images/thumb/4/4b/French_traction_power_substation.jpg/320px-French_traction_power_substation.jpg",
	"8020": "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Post_office_Wenigzell%2C_Styria.jpg/320px-Post_office_Wenigzell%2C_Styria.jpg",
	"8021": "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Post_office_Wenigzell%2C_Styria.jpg/320px-Post_office_Wenigzell%2C_Styria.jpg",
	"8040": "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/BYD_K-9_electric_test_bus_0060_in_NYC.jpg/320px-BYD_K-9_electric_test_bus_0060_in_NYC.jpg",
	"8041": "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/BYD_K-9_electric_test_bus_0060_in_NYC.jpg/320px-BYD_K-9_electric_test_bus_0060_in_NYC.jpg",
	"8050": "https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Amay_-_Gare_de_Amay1.jpg/320px-Amay_-_Gare_de_Amay1.jpg",
	"8051": "https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Amay_-_Gare_de_Amay1.jpg/320px-Amay_-_Gare_de_Amay1.jpg",
	"8060": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/France_road_sign_A8.svg/320px-France_road_sign_A8.svg.png",
	"8080": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Num%C3%A9ro_121%2C_Rue_de_Vaugirard_%28Paris%29.jpg/479px-Num%C3%A9ro_121%2C_Rue_de_Vaugirard_%28Paris%29.jpg",
	"8120": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/N%C3%A1b%C5%99e%C5%BE%C3%AD_J._Palacha_-_Foersterova%2C_kontejnery.jpg/320px-N%C3%A1b%C5%99e%C5%BE%C3%AD_J._Palacha_-_Foersterova%2C_kontejnery.jpg",
	"8121": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/N%C3%A1b%C5%99e%C5%BE%C3%AD_J._Palacha_-_Foersterova%2C_kontejnery.jpg/320px-N%C3%A1b%C5%99e%C5%BE%C3%AD_J._Palacha_-_Foersterova%2C_kontejnery.jpg",
	"8122": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/N%C3%A1b%C5%99e%C5%BE%C3%AD_J._Palacha_-_Foersterova%2C_kontejnery.jpg/320px-N%C3%A1b%C5%99e%C5%BE%C3%AD_J._Palacha_-_Foersterova%2C_kontejnery.jpg",
	"8130": "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Tel_Aviv_parking_lot.jpg/320px-Tel_Aviv_parking_lot.jpg",
	"8131": "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Tel_Aviv_parking_lot.jpg/320px-Tel_Aviv_parking_lot.jpg",
	"8132": "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Tel_Aviv_parking_lot.jpg/320px-Tel_Aviv_parking_lot.jpg",
	"8150": "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/AlewifeBikeParking.agr.2001.JPG/320px-AlewifeBikeParking.agr.2001.JPG",
	"8180": "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Toaleta_inteligenta_in_Bucharest.jpg/378px-Toaleta_inteligenta_in_Bucharest.jpg",
	"8210": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Znak_%C4%8Cesk%C3%A9_l%C3%A9k%C3%A1rensk%C3%A9_komory.JPG/320px-Znak_%C4%8Cesk%C3%A9_l%C3%A9k%C3%A1rensk%C3%A9_komory.JPG",
	"8211": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Znak_%C4%8Cesk%C3%A9_l%C3%A9k%C3%A1rensk%C3%A9_komory.JPG/320px-Znak_%C4%8Cesk%C3%A9_l%C3%A9k%C3%A1rensk%C3%A9_komory.JPG",
	"8230": "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3b/SteacieLibrary.jpg/320px-SteacieLibrary.jpg",
	"8240": "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Plant_interior.jpg/320px-Plant_interior.jpg",
	"8280": "https://wiki.openstreetmap.org/w/images/thumb/4/4b/French_traction_power_substation.jpg/320px-French_traction_power_substation.jpg",
	"8281": "https://wiki.openstreetmap.org/w/images/thumb/4/4b/French_traction_power_substation.jpg/320px-French_traction_power_substation.jpg",
	"8290": "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e2/Top_of_power_line_pole_-_west_side.jpg/315px-Top_of_power_line_pole_-_west_side.jpg"
};

module.exports = OsmoseDataset;

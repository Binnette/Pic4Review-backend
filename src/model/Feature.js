const jsts = require('jsts');

const STATUSES = [ "new", "skipped", "nopics", "reviewed", "cantsee" ];

/**
 * A feature is a geolocated object, which has to reviewed using pictures.
 * It has a review status, center coordinates, and a list of pictures associated.
 * 
 * @param {string} id An unique ID between all features of a given {@link Dataset}
 * @param {float[]} coordinates The feature center coordinates, as [ lat, lng ] in WGS84
 * @param {Object} properties A list of key->value properties from data source
 * @param {string} [status] The feature review status, one of [new, skipped, nopics, reviewed]. Defaults to "new".
 * @param {Geometry} [geom] The geometry in JTS format
 * 
 * @property {string} id The feature unique ID
 * @property {float[]} coordinates The feature coordinates as [lat, lng] in WGS84
 * @property {Object} properties The feature properties, as a set of key -> value
 * @property {Geometry} geom The feature geometry in JSTS format
 */
class Feature {
	constructor(id, coordinates, properties, status, geom) {
		if(id == null || id === "") {
			throw new TypeError("ID must be a valid string");
		}
		
		if(!Array.isArray(coordinates) || coordinates.length !== 2 || isNaN(coordinates[0]) || isNaN(coordinates[1])) {
			throw new TypeError("Coordinates must be a float array as [ lat, lng ]");
		}
		
		this.id = id;
		this.coordinates = coordinates;
		this.properties = properties;
		this.status = status || "new";
		
		if(geom && geom.isValid()) {
			this.geom = geom;
		}
		else {
			const factory = new jsts.geom.GeometryFactory();
			this.geom = factory.createPoint(new jsts.geom.Coordinate(coordinates[1], coordinates[0]));
		}
	}


//ACCESSORS

	/**
	 * Get the review status of this feature.
	 * @return {string} The status: new, skipped, nopics, reviewed
	 */
	get status() {
		return this._status;
	}


//MODIFIERS

	/**
	 * Set the status of the feature
	 * @param {string} value The new status value (one of [new, skipped, nopics, reviewed]).
	 */
	set status(value) {
		if(STATUSES.indexOf(value) >= 0) {
			this._status = value;
		}
		else {
			throw new TypeError("Invalid status value for feature "+this.id+" : "+value);
		}
	}

//OTHER METHODS

	/**
	 * Get the feature as a GeoJSON point feature.
	 * @return {Object} The GeoJSON representation of this feature.
	 */
	asGeoJSON() {
		const props = Object.assign({}, this.properties);
		
		const geojson = {
			type: "Feature",
			geometry: (new jsts.io.GeoJSONWriter()).write(this.geom),
			properties: props
		};
		
		return geojson;
	}
}

Feature.STATUSES = STATUSES;

module.exports = Feature;

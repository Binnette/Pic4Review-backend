/**
 * Route handler for users-related routes.
 * @name UsersRouteHandler
 */

/**
 * Handle /users/:uid/stats requests.
 */
exports.stats = function(req, res) {
	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.params) {
		res.status(400).send({ "error": "Missing parameters" });
	}
	else if(!req.params.uid) {
		res.status(400).send({ "error": "Invalid user ID" });
	}
	else {
		const result = {};
		const promises = [];
		
		//Amount of edited objects
		promises.push(
			db.query("SELECT COUNT(DISTINCT feature) AS cnt FROM edit WHERE userid = $1", [ req.params.uid ])
			.then(d => {
				if(d.rows.length === 1) {
					result.featuresEdited = parseInt(d.rows[0].cnt);
					return true;
				}
				else {
					throw new Error("No user found");
				}
			})
		);
		
		//Amount of created missions
		promises.push(
			db.query("SELECT COUNT(*) AS cnt FROM mission WHERE userid = $1", [ req.params.uid ])
			.then(d => {
				if(d.rows.length === 1) {
					result.missionsCreated = parseInt(d.rows[0].cnt);
					return true;
				}
				else {
					throw new Error("No user found");
				}
			})
		);
		
		//Most edited themes
		promises.push(
			db.query("SELECT m.theme AS theme, COUNT(*) AS cnt FROM (SELECT DISTINCT feature, mission FROM edit WHERE userid = $1) e JOIN mission m on e.mission = m.id GROUP BY theme", [ req.params.uid ])
			.then(d => {
				result.themes = {};
				
				d.rows.forEach(l => {
					result.themes[l.theme] = parseInt(l.cnt);
				});
				
				return true;
			})
		);
		
		//User score
		promises.push(
			db.query("SELECT place FROM score WHERE userid = $1", [ req.params.uid ])
			.then(d => {
				result.place = null;
				
				if(d.rows.length === 1) {
					result.place = parseInt(d.rows[0].place);
				}
				
				return true;
			})
		);
		
		//User edits during last 3 months
		promises.push(
			db.query("SELECT moment::date AS day, count(*) AS cnt FROM edit WHERE userid = $1 AND age(current_timestamp, moment) <= '3 months' GROUP BY day", [ req.params.uid ])
			.then(d => {
				result.amountEdits = {};
				
				d.rows.forEach(l => {
					result.amountEdits[l.day.toP4R()] = parseInt(l.cnt);
				});
				
				return true;
			})
		);
		
		Promise.all(promises)
		.then(v => {
			res.status(200).send(result);
		})
		.catch(e => {
			res.status(500).send({ error: e });
		});
	}
};

/**
 * Handle /users/stats requests.
 */
exports.allStats = function(req, res) {
	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(req.query && req.query.user && isNaN(parseInt(req.query.user))) {
		res.status(400).send({ "error": "Invalid user ID" });
	}
	else {
		const result = {};
		const promises = [];
		const userId = req.query.user || null;
		
		//Users scores
		promises.push(
			db.query("SELECT * FROM score ORDER BY place LIMIT 20")
			.then(d => {
				result.scores = d.rows.map(l => {
					return { user: l.username, featuresEdited: parseInt(l.cnt), place: parseInt(l.place) };
				});
				
				//Retrieve user rank if not in top 20
				if(userId && d.rows.filter(l => l.userid === userId).length === 0) {
					return db.query("SELECT * FROM score WHERE userid = $1", [ userId ])
					.then(d2 => {
						if(d2.rows.length === 1) {
							const ud = d2.rows[0];
							result.scores.push({ user: ud.username, featuresEdited: parseInt(ud.cnt), place: parseInt(ud.place) });
						}
						
						return true;
					});
				}
				else {
					return true;
				}
			})
		);
		
		//Global amount of edits
		promises.push(
			db.query("SELECT moment::date AS day, count(*) AS cnt FROM edit WHERE age(current_timestamp, moment) <= '3 months' GROUP BY day")
			.then(d => {
				result.amountEdits = {};
				
				d.rows.forEach(l => {
					result.amountEdits[l.day.toP4R()] = parseInt(l.cnt);
				});
				
				return true;
			})
		);
		
		//Mission themes
		promises.push(
			db.query("SELECT theme, COUNT(*) AS cnt FROM mission GROUP BY theme")
			.then(d => {
				result.themes = {};
				d.rows.forEach(l => {
					result.themes[l.theme] = l.cnt;
				});
				
				return true;
			})
		);
		
		//Pictures providers
		promises.push(
			db.query("SELECT * FROM picture_provider_cache")
			.then(d => {
				result.picProviders = {};
				d.rows.forEach(l => {
					result.picProviders[l.provider] = l.cnt;
				});
				
				return true;
			})
		);
		
		Promise.all(promises)
		.then(v => {
			res.status(200).send(result);
		})
		.catch(e => {
			res.status(500).send({ error: e });
		});
	}
};

/**
 * Handle /users/dataviz requests.
 */
exports.dataviz = function(req, res) {
	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else {
		const result = {};
		const promises = [];
		
		//Amount of missions
		promises.push(
			db.query("SELECT COUNT(*)::int AS cnt FROM mission_rank_cache WHERE status = 'online'")
			.then(d => {
				result.missions = d.rows[0].cnt;
				return true;
			})
		);
		
		//Amount of edits
		promises.push(
			db.query("SELECT COUNT(*)::int AS cnt FROM edit WHERE newstatus = 'reviewed'")
			.then(d => {
				result.edits = d.rows[0].cnt;
				return true;
			})
		);
		
		//Amount of users
		promises.push(
			db.query("SELECT COUNT(DISTINCT userid)::int AS cnt FROM username")
			.then(d => {
				result.users = d.rows[0].cnt;
				return true;
			})
		);
		
		Promise.all(promises)
		.then(v => {
			res.status(200).send(result);
		})
		.catch(e => {
			res.status(500).send({ error: e });
		});
	}
};

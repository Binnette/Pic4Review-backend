/**
 * Route handler for pictures-related routes.
 * @name PicturesRouteHandler
 */

/**
 * Handle /pictures/missing requests.
 */
exports.missing = function(req, res) {
	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else {
		//Amount of edited objects
		db.query("SELECT DISTINCT ST_AsGeoJSON(geom) AS geom FROM (SELECT * FROM feature WHERE status IN ('cantsee', 'nopics') ORDER BY lastedit DESC) a LIMIT 2000")
		.then(d => {
			const result = {
				type: "FeatureCollection",
				features: d.rows.map(l => { return { type: "Feature", geometry: JSON.parse(l.geom), properties: { date: l.lastedit } }; })
			};
			
			res.status(200).send(result);
		})
		.catch(e => {
			console.error(e);
			res.status(500).send({ "error": "Failed to retrieve missing pictures", "details_for_humans": "I'm not able to find features lacking pictures for now, can you retry a bit later ?" });
		});
	}
};

const CONFIG = require("../../config.json");
const mailer = require("nodemailer");

/**
 * Mail manager allows to send email about application status.
 */
class MailManager {
	constructor() {
		this.transporter = mailer.createTransport({
			host: CONFIG.mail.from.server,
			auth: {
				user: CONFIG.mail.from.address,
				pass: CONFIG.mail.from.password
			},
			secure: CONFIG.mail.from.secure,
			tls: {
				rejectUnauthorized: false
			}
		});
		
		this.transporter.verify((error, success) => {
			if(error) {
				throw new Error(error);
			}
		});
	}
	
	/**
	 * Send an email
	 * @param {string} subject The mail subject
	 * @param {string} content The mail content as plain text
	 * @param {string} [dest] The mail destination, defaults to CONFIG.mail.to.default if not defined
	 * @return {Promise} Promise resolving on mail sent
	 */
	send(subject, content, dest) {
		const msg = {
			from: CONFIG.mail.from.address,
			to: dest ? dest : CONFIG.mail.to.default,
			subject: subject,
			text: content
		};
		
		return this.transporter.sendMail(msg);
	}
}

module.exports = MailManager;

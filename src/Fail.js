const CONFIG = require('../config.json');
const MailManager = require('./ctrl/MailManager');

/**
 * Sends a mail saying that API failed
 */

const mail = new MailManager();
mail.send("Pic4Review API is in Null Island", "Pic4Review API has just stopped after 3 restarts, please restart it manually :-)")
.then(() => {
	console.log("Administrator of API was alerted");
})
.catch(err => {
	console.error("Can't alert administrator...");
});
